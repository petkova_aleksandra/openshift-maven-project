#!/bin/bash

echo "************************"
echo "*****Running tests******"
echo "************************"

WORKSPACE=/home/peta/jenkins/jenkins-data/jenkins_home/workspace/openshift-maven-project

docker run --rm -v $WORKSPACE/java-app:/app -v /root/.m2:/root/.m2 -w /app maven:3-alpine mvn test

rm -rf $WORKSPACE/java-app/target


