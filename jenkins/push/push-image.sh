#!/bin/bash

echo "**********************"
echo "****Pushing image*****"
echo "**********************"

IMAGE_NAME="openshift-project"

echo "********Login*********"
docker login -u petkova94 -p $DOCKER_HUB_PASSWORD

echo "****Tagging image*****"
docker tag $IMAGE_NAME:$BUILD_TAG petkova94/$IMAGE_NAME:$BUILD_TAG

echo "****Pushing image*****"
docker push petkova94/$IMAGE_NAME:$BUILD_TAG
